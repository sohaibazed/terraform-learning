output "Bucket Name" {
    value = "${module.storage.bucketname}"
}

output "Public Subnets" {
    value = "${join(", ", module.networking.public_subnets)}"
}

output "Subnets IPs" {
    value = "${join(", " , module.networking.subnet_ips)}"
}

output "Public SG" {
    value = "${module.networking.public_sg}"
}

output "Server_IDs" {
    value = "${module.compute.server_id}"
    
}

output "Server IPs" {
    value = "${module.compute.server_ip}"
    
}